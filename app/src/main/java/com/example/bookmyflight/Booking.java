package com.example.bookmyflight;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class Booking extends AppCompatActivity {

    private TextView origin,destination,originTime,destimationtime,fare,providerId,airlinecode,bookingClass;
    private ArrayList<FilterData> filterDatas = new ArrayList<>();
    private  int id;
    private  FilterData filterData;
    private HashMap<Integer,String> providerDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        Bundle bundle = getIntent().getExtras();
        if(getIntent() != null){
//            useful when need to pass list of object
//            filterDatas=  bundle.getParcelableArrayList("filterdataList");
            filterData = bundle.getParcelable("data");
            id = bundle.getInt("position");

        }
        initilizeView();

    }

    private void initilizeView() {
        origin = findViewById(R.id.origin);
        destination = findViewById(R.id.destination);
        originTime = findViewById(R.id.originTime);
        destimationtime = findViewById(R.id.destinationtime);
        fare = findViewById(R.id.fare);
        providerId = findViewById(R.id.provider);
        airlinecode = findViewById(R.id.airlinesCode);
        bookingClass = findViewById(R.id.bookingClass);
        providerDescription = Utility.getEntry();
//        FilterData selectedFlightData = filterDatas.get(id);

        if (filterData != null) {
            origin.setText(filterData.getOriginCode());
            destination.setText(filterData.getDestinationCode());
            Utility.setTime(originTime, filterData.getDepartureTime());
            Utility.setTime(destimationtime, filterData.getArrivalTime());
            fare.setText("Rs. " + filterData.getFares());
            providerId.setText(providerDescription.get((int)filterData.getProviderId()));
            airlinecode.setText(filterData.getAirlineCode());
            bookingClass.setText(filterData.getBookingClass());
        }
    }



}
