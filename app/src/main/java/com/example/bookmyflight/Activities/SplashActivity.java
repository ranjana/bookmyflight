package com.example.bookmyflight.Activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.bookmyflight.MainActivity;
import com.example.bookmyflight.Utility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.bookmyflight.R;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME = 1000;
    private  ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageView =(ImageView) findViewById(R.id.icon);
        rotateIcon();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Utility.getBooleandataFromPreferance(SplashActivity.this,Constants.LOGGEDIN)){
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                SplashActivity.this.finish();
            }

        },SPLASH_TIME);

    }

    private void rotateIcon() {
        RotateAnimation rotate = new RotateAnimation(30, 360, Animation.RELATIVE_TO_SELF, 0.5f,  Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(SPLASH_TIME);
        imageView.startAnimation(rotate);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
