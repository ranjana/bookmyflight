package com.example.bookmyflight.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.bookmyflight.R;
import com.example.bookmyflight.Utility;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class ProfileActivity extends AppCompatActivity {

    private TextView email,name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initializeView();
        setData();
    }

    private void initializeView() {
        email = findViewById(R.id.email);
        name = findViewById(R.id.name);

    }

    private void setData() {
        if(Utility.getStringdataFromPreferance(this,Constants.EMAIL) != "") {
            email.setText(Utility.getStringdataFromPreferance(this, Constants.EMAIL));
        }
        if(Utility.getStringdataFromPreferance(this,Constants.NAME) != "") {
            name.setText(Utility.getStringdataFromPreferance(this,Constants.NAME));
        }

    }
}
