package com.example.bookmyflight.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.bookmyflight.MainActivity;
import com.example.bookmyflight.Utility;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.bookmyflight.R;

/**
 * @author ranjana.rani on 09/09/19.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public int RC_SIGN_IN = 999;
    public static GoogleSignInClient mGoogleSignInClient;
    public static GoogleSignInOptions gso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        connectGSO(this);

    }

    private static void connectGSO(Activity activity) {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                if(Utility.ifInternetAvailable(this)) {
                    signIn();
                }else {
                    Toast.makeText(this,"Cant login, make sure you are connected to Internet",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            updateUI(account);
            Utility.savedataInPreferance(this,Constants.LOGGEDIN,true);
        } catch (ApiException e) {
            Log.w("LoginActivity", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    public static void signOut(final Activity activity) {
        connectGSO(activity);
        if(mGoogleSignInClient == null){
            Toast.makeText(activity,"cant logout",Toast.LENGTH_LONG).show();
            return;
        }
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Utility.savedataInPreferance(activity,Constants.LOGGEDIN,false);
                        Intent intent = new Intent(activity,SplashActivity.class);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                });
        Utility.deleteDB(activity);
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            Intent intent = new Intent(this, MainActivity.class);
            Utility.saveStringdataInPreferance(this,Constants.NAME, account.getEmail());
            Utility.saveStringdataInPreferance(this,Constants.EMAIL,account.getDisplayName());
            startActivity(intent);
        }else{
            Toast.makeText(this,"error in login",Toast.LENGTH_LONG).show();
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
