package com.example.bookmyflight;

import java.util.HashMap;
import java.util.Map;
/**
 * @author ranjana.rani on 09/09/19.
 */
public enum  providerEnum {
    MAKE_MY_TRIP(1, "MakeMyTrip"), CLEARTRIP(2, "Cleartrip"), YATRA(3, "Yatra"), MUSAFIR(4, "Musafir");

    private final int id;
    private final String description;

    private providerEnum(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int id() {
        return id;
    }

    public String description() {
        return description;
    }

    public static providerEnum valueOf(long id) {
        return stages.get(id);
    }
    private static final Map<Integer, providerEnum> stages = new HashMap<>();

    static {
        for (providerEnum status : providerEnum.values()) {
            if (stages.get(status.id) == null) {
                stages.put(status.id, status);
            }
        } 
    }

}
