package com.example.bookmyflight;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author ranjana.rani on 09/09/19.
 */
class Fare {
    private long providerId;
    private  long fare;

    @JsonProperty("providerId")
    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }
    @JsonProperty("fare")
    public long getFare() {
        return fare;
    }
    public void setFare(long fare) {
        this.fare = fare;
    }

}
