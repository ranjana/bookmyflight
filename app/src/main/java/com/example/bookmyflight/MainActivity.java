package com.example.bookmyflight;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookmyflight.Activities.LoginActivity;
import com.example.bookmyflight.Activities.ProfileActivity;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
// String url1 = "{\"appendix\": {\"airlines\": {\"SG\": \"Spicejet\",\"AI\": \"Air India\",\"G8\": \"Go Air\",\"9W\": \"Jet Airways\",\"6E\": \"Indigo\"},\"airports\":" +
////                    " {\"DEL\": \"New Delhi\",\"BOM\": \"Mumbai\"}," +
////                    "\"providers\": {\"1\": \"MakeMyTrip\",\"2\": \"Cleartrip\",\"3\": \"Yatra\",\"4\": \"Musafir\"}},\"flights\": [{\"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396614600000, \"arrivalTime\": 1396625400000, \"fares\": [{ \"providerId\": 1, \"fare\": 11500 }, { \"providerId\": 2, \"fare\": 10500 }], \"airlineCode\": \"G8\", \"bookingClass\": \"Business\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396616400000, \"arrivalTime\": 1396623600000, \"fares\": [{ \"providerId\": 1, \"fare\": 14400 }, { \"providerId\": 3, \"fare\": 14000 }], \"airlineCode\": \"AI\", \"bookingClass\": \"Business\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396618200000, \"arrivalTime\": 1396629000000, \"fares\": [{ \"providerId\": 2, \"fare\": 12300 }], \"airlineCode\": \"9W\", \"bookingClass\": \"Business\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396620000000, \"arrivalTime\": 1396627200000, \"fares\": [{ \"providerId\": 2, \"fare\": 15200 }, { \"providerId\": 3, \"fare\": 15000 }], \"airlineCode\": \"SG\", \"bookingClass\": \"Business\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396621800000, \"arrivalTime\": 1396632600000, \"fares\": [{ \"providerId\": 1, \"fare\": 16000 }, { \"providerId\": 4, \"fare\": 16700 }], \"airlineCode\": \"6E\", \"bookingClass\": \"Business\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396596600000, \"arrivalTime\": 1396607400000, \"fares\": [{ \"providerId\": 1, \"fare\": 5500 }, { \"providerId\": 2, \"fare\": 5600 }], \"airlineCode\": \"G8\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396598400000, \"arrivalTime\": 1396605600000, \"fares\": [{ \"providerId\": 1, \"fare\": 4400 }, { \"providerId\": 2, \"fare\": 4400 }, { \"providerId\": 3, \"fare\": 4500 }, { \"providerId\": 4, \"fare\": 4600 }], \"airlineCode\": \"AI\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396600200000, \"arrivalTime\": 1396611000000, \"fares\": [{ \"providerId\": 1, \"fare\": 4600 }, { \"providerId\": 4, \"fare\": 4700 }], \"airlineCode\": \"9W\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396602000000, \"arrivalTime\": 1396609200000, \"fares\": [{ \"providerId\": 3, \"fare\": 7800 }, { \"providerId\": 4, \"fare\": 7700 }], \"airlineCode\": \"SG\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396603800000, \"arrivalTime\": 1396614600000, \"fares\": [{ \"providerId\": 2, \"fare\": 8600 }, { \"providerId\": 3, \"fare\": 8700 }], \"airlineCode\": \"6E\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396605600000, \"arrivalTime\": 1396612800000, \"fares\": [{ \"providerId\": 2, \"fare\": 5000 }], \"airlineCode\": \"G8\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396607400000, \"arrivalTime\": 1396618200000, \"fares\": [{ \"providerId\": 2, \"fare\": 9800 }, { \"providerId\": 3, \"fare\": 10000 }], \"airlineCode\": \"AI\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396609200000, \"arrivalTime\": 1396616400000, \"fares\": [{ \"providerId\": 1, \"fare\": 4100 }, { \"providerId\": 2, \"fare\": 4200 }], \"airlineCode\": \"9W\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396611000000, \"arrivalTime\": 1396621800000, \"fares\": [{ \"providerId\": 4, \"fare\": 4600 }], \"airlineCode\": \"SG\", \"bookingClass\": \"Economy\" }, { \"originCode\": \"DEL\", \"destinationCode\": \"BOM\", \"departureTime\": 1396612800000, \"arrivalTime\": 1396620000000, \"fares\": [{ \"providerId\": 1, \"fare\": 5700 }, { \"providerId\": 3, \"fare\": 5500 }], \"airlineCode\": \"6E\", \"bookingClass\": \"Economy\"}]}";
//
/**
 * @author ranjana.rani on 09/09/19.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<Flight> flights = new ArrayList<>();
    private ArrayList<FilterData> filterDatas;
    private HashMap<Integer, String> providerDescription = new HashMap<>();
    private FlightListAdapter adapter;
    private RecyclerView recyclerView;
    private String url = "http://www.mocky.io/v2/5979c6731100001e039edcb3";
    private ImageView profile;
    private TextView noInternet;
    private Spinner spinner;
    private Group internetgroup;
    private TextView refresh;
    private ProgressBar progressbar;
    private ArrayList<String> sorlist;
    //mylocalhost
    private String mylocalhosturl = "http://localhost:8080/BuslocationTracker/webresources/myresource/test";
    private HttpNetworkCall.OnTaskCompleted onTaskCompletedlistener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();
        Log.d("called","oncreate");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("called","onPostResume");



    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("called","onStart");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("called","onStop");

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("laststireddata",filterDatas);
        Log.d("called","onSaveInstanceState");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("called","onPause");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("called","onResume");
        if(filterDatas == null || filterDatas.size() == 0){
            checkAndLoadMainActivity();
        }else{
            setdataInListView();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("called","onRestart");


    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("called","onRestoreInstanceState");
        filterDatas = savedInstanceState.getParcelableArrayList("laststireddata");
    }

    private void checkAndLoadMainActivity() {
        filterDatas = Utility.getAlldataFromDB(MainActivity.this);
        if(filterDatas != null && filterDatas.size()>0){
            setdataInListView();
        }else if(!Utility.ifInternetAvailable(this)){
            progressbar.setVisibility(View.GONE);
            noInternet.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.VISIBLE);
            findViewById(R.id.internetgroup).setVisibility(View.GONE);
            Toast.makeText(this,"Cant load, Make sure you are connected to internet",Toast.LENGTH_LONG).show();
        } else{
            requestDataAndInsertTodataBase();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);

    }

    private void requestDataAndInsertTodataBase() {
        new HttpNetworkCall(url, new HttpNetworkCall.OnTaskCompleted() {
                @Override
                public void onSuccessfullCompetion(String json) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    try {
                        progressbar.setVisibility(View.GONE);
                        noInternet.setVisibility(View.GONE);
                        refresh.setVisibility(View.GONE);
                        findViewById(R.id.internetgroup).setVisibility(View.VISIBLE);
                        FlightDetailAllData dummyClass = objectMapper.readValue(json, FlightDetailAllData.class);
                        if (dummyClass != null) {
                            flights = dummyClass.getFlights();
                            Utility.getlistOfData(MainActivity.this, flights);
                            filterDatas = Utility.getAlldataFromDB(MainActivity.this);
                            setdataInListView();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure() {
                    progressbar.setVisibility(View.GONE);
                    noInternet.setVisibility(View.VISIBLE);
                    refresh.setVisibility(View.VISIBLE);
                    findViewById(R.id.internetgroup).setVisibility(View.INVISIBLE);

                }
            }).execute();
    }

    private void setdataInListView(){
        adapter = new FlightListAdapter(this, filterDatas, providerDescription);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        sorlist = Utility.getSpinnerList();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sorlist);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sortBy = parent.getItemAtPosition(position).toString();
                switch (sortBy) {
                    case "price":
                        Collections.sort(filterDatas, new PriceSorter());
                        adapter.notifyDataSetChanged();
                        break;
                    case "departureTime":
                        Collections.sort(filterDatas, new DepartureTimeSorter());
                        adapter.notifyDataSetChanged();
                        break;
                    case "arrivaltime":
                        Collections.sort(filterDatas, new ArrivalTimeSorter());
                        adapter.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initializeView() {
        recyclerView = findViewById(R.id.recyclerview);
        profile = findViewById(R.id.profile);
        TextView signOut = findViewById(R.id.signOut);
        spinner = findViewById(R.id.sortBy);
        noInternet = findViewById(R.id.noInternet);
        refresh = findViewById(R.id.refresh);
        progressbar = findViewById(R.id.progressbar);
        signOut.setOnClickListener(this);
        profile.setOnClickListener(this);
        refresh.setOnClickListener(this);
        providerDescription = Utility.getEntry();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signOut:
                showLogoutDiallog(this);
                break;
            case R.id.profile:
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.refresh:
                progressbar.setVisibility(View.VISIBLE);
                noInternet.setVisibility(View.GONE);
                refresh.setVisibility(View.GONE);
                checkAndLoadMainActivity();
                break;


        }
    }

    private void showLogoutDiallog(final Activity activity) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("SignOut");
        alertDialog.setMessage("DO you want to logout");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(Utility.ifInternetAvailable(activity)) {
                    LoginActivity.signOut(activity);

                }else {
                    Toast.makeText(activity,"Cant logOut, make sure you are connected to Internet",Toast.LENGTH_LONG).show();
                }
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();


    }


    public class DepartureTimeSorter implements Comparator<FilterData> {
        @Override
        public int compare(FilterData o1, FilterData o2) {
            return (int) (o1.getDepartureTime() - o2.getDepartureTime());
        }
    }

    public class PriceSorter implements Comparator<FilterData> {
        @Override
        public int compare(FilterData o1, FilterData o2) {
            return (int) (o1.getFares() - o2.getFares());
        }
    }

    public class ArrivalTimeSorter implements Comparator<FilterData> {
        @Override
        public int compare(FilterData o1, FilterData o2) {
            return (int) (o1.getArrivalTime() - o2.getArrivalTime());
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("called","onDestroy");
        sorlist.clear();

    }
}
