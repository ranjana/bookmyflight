package com.example.bookmyflight;



import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
/**
 * @author ranjana.rani on 09/09/19.
 */
class FlightDetailAllData {
    private Appendix appendix;
    private ArrayList<Flight> flights;
    @JsonProperty("appendix")
    public Appendix getAppendix() {
        return appendix;
    }

    public void setAppendix(Appendix appendix) {
        this.appendix = appendix;
    }
    @JsonProperty("flights")
    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }
}
