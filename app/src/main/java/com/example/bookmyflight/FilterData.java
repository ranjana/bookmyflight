package com.example.bookmyflight;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class FilterData implements Parcelable{
    private  String originCode;
    private  String destinationCode;
    private  long departureTime;
    private  long arrivalTime;
    private  long fares;
    private long providerId;
    private  String airlineCode;
    private String bookingClass;

    public FilterData(String originCode, String  destinationCode){
        this.originCode = originCode;
        this.destinationCode = destinationCode;

    }

    public FilterData(String originCode, String destinationCode, long departureTime, long arrivalTime,
                      long fares, long providerId, String airlineCode, String bookingClass) {
        this.originCode = originCode;
        this.destinationCode = destinationCode;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.fares = fares;
        this.providerId = providerId;
        this.airlineCode = airlineCode;
        this.bookingClass = bookingClass;
    }

    protected FilterData(Parcel in) {
        originCode = in.readString();
        destinationCode = in.readString();
        departureTime = in.readLong();
        arrivalTime = in.readLong();
        fares = in.readLong();
        providerId = in.readLong();
        airlineCode = in.readString();
        bookingClass = in.readString();
    }

    public static final Creator<FilterData> CREATOR = new Creator<FilterData>() {
        @Override
        public FilterData createFromParcel(Parcel in) {
            return new FilterData(in);
        }

        @Override
        public FilterData[] newArray(int size) {
            return new FilterData[size];
        }
    };

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public long getFares() {
        return fares;
    }

    public void setFares(long fares) {
        this.fares = fares;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(originCode);
        parcel.writeString(destinationCode);
        parcel.writeLong(departureTime);
        parcel.writeLong(arrivalTime);
        parcel.writeLong(fares);
        parcel.writeLong(providerId);
        parcel.writeString(airlineCode);
        parcel.writeString(bookingClass);
    }
}
