package com.example.bookmyflight;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author ranjana.rani on 09/09/19.
 */
class Airlines {
    private String SG;
    private String AI;
    private String G8;
    private String w;
    private String e;

    @JsonProperty("SG")
    public String getSG() {
        return SG;
    }

    public void setSG(String SG) {
        this.SG = SG;
    }

    @JsonProperty("AI")
    public String getAI() {
        return AI;
    }

    public void setAI(String AI) {
        this.AI = AI;
    }
    @JsonProperty("G8")
    public String getG8() {
        return G8;
    }

    public void setG8(String g8) {
        G8 = g8;
    }
    @JsonProperty("9W")
    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }
    @JsonProperty("6E")
    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }
}
