package com.example.bookmyflight;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class FlightListAdapter extends RecyclerView.Adapter<FlightListAdapter.ViewHolder> {
    private  ArrayList<FilterData> filterData;
    private HashMap<Integer,String> providerDescription;
    private Context context;
    public FlightListAdapter(Context context,ArrayList<FilterData> filterData, HashMap<Integer,String> providerDescription) {
        this.providerDescription = providerDescription;
        this.filterData = filterData;
        this.context = context;
    }


    @NonNull
    @Override
    public FlightListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.list_parent_layout,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final FlightListAdapter.ViewHolder holder, final int position) {

        final FilterData flight=filterData.get(position);
        holder.provider.setText(providerDescription.get((int)flight.getProviderId()));
        holder.price.setText("Rs. "+flight.getFares());
        Utility.setTime(holder.departureTime,flight.getDepartureTime());
        Utility.setTime(holder.arrivalTime,flight.getArrivalTime());

        holder.bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Booking.class);
                Bundle bundle = new Bundle();
                intent.putExtra("data",flight);
//                bundle.putParcelableArrayList("filterdataList",filterData);
                bundle.putInt("position",position);
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return filterData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView provider;
        TextView departureTime;
        TextView arrivalTime;
        Button bookNow;
        TextView price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.provider = itemView.findViewById(R.id.provider);
            this.departureTime = itemView.findViewById(R.id.departureTime);
            this.arrivalTime = itemView.findViewById(R.id.arrivalTime);
            this.bookNow = itemView.findViewById(R.id.booknow);
            this.price = itemView.findViewById(R.id.price);

        }
    }
}
