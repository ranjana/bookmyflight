package com.example.bookmyflight;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.UUID;

import static android.os.Build.ID;
/**
 * @author ranjana.rani on 09/09/19.
 */
class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "flightdetails.db";
    public static final String TABLE_NAME = "flight_detail";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "ORIGIN_CODE";
    public static final String COL_3 = "DESTINATION_CODE";
    public static final String COL_4 = "DEPARTURE_TIME";
    public static final String COL_5 = "ARRIVAL_TIME";
    public static final String COL_6 = "FARES";
    public static final String COL_7 = "PROVIDER_ID";
    public static final String COL_8 = "AIRLINE_CODE";
    public static final String COL_9 = "BOOKING_CLASS";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,ORIGIN_CODE TEXT," +
                "DESTINATION_CODE TEXT,DEPARTURE_TIME INTEGER,ARRIVAL_TIME INTEGER,FARES INTEGER,PROVIDER_ID INTEGER," +
                "AIRLINE_CODE TEXT,BOOKING_CLASS TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public  boolean insertData(String originCode,String destinationCode,long departuretime,long arrivaltime, long fares,long providerId,
                              String airlineCode, String bookingClass) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,originCode);
        contentValues.put(COL_3,destinationCode);
        contentValues.put(COL_4,departuretime);
        contentValues.put(COL_5,arrivaltime);
        contentValues.put(COL_6,fares);
        contentValues.put(COL_7,providerId);
        contentValues.put(COL_8,airlineCode);
        contentValues.put(COL_9,bookingClass);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public ArrayList<FilterData> getAllData(){
        ArrayList<FilterData> filterDatas= new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()) {
            do {
                filterDatas.add(new FilterData(cursor.getString(cursor.getColumnIndex(COL_2)),cursor.getString(cursor.getColumnIndex(COL_3)),
                        cursor.getLong(cursor.getColumnIndex(COL_4)),cursor.getLong(cursor.getColumnIndex(COL_5)),cursor.getLong(cursor.getColumnIndex(COL_6)),
                        cursor.getLong(cursor.getColumnIndex(COL_7)), cursor.getString(cursor.getColumnIndex(COL_8)), cursor.getString(cursor.getColumnIndex(COL_9))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return filterDatas;
    }

    public FilterData getAllDataById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT  * FROM ").append(TABLE_NAME)
                .append(" WHERE ").append(COL_1).append(" = ").append(id)
                .append(" ; ");
        Cursor cursor = db.rawQuery(sb.toString(), null);
        return getAllFlightData(cursor);
    }

    private FilterData getAllFlightData(Cursor cursor) {
      FilterData filterData = null;
            if (cursor != null && cursor.moveToFirst()) {
                filterData = new FilterData(cursor.getString(cursor.getColumnIndex(COL_2)),cursor.getString(cursor.getColumnIndex(COL_3)),
                        cursor.getLong(cursor.getColumnIndex(COL_4)),cursor.getLong(cursor.getColumnIndex(COL_5)),cursor.getLong(cursor.getColumnIndex(COL_6)),
                                cursor.getLong(cursor.getColumnIndex(COL_7)), cursor.getString(cursor.getColumnIndex(COL_8)), cursor.getString(cursor.getColumnIndex(COL_9)));
            }
        return filterData;
    }

    public boolean updateData(String id,String originCode,String destinationCode,long departuretime,long arrivaltime, long fares,long providerId,
                              String airlineCode, String bookingClass) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,originCode);
        contentValues.put(COL_3,destinationCode);
        contentValues.put(COL_4,departuretime);
        contentValues.put(COL_5,arrivaltime);
        contentValues.put(COL_6,fares);
        contentValues.put(COL_7,providerId);
        contentValues.put(COL_8,airlineCode);
        contentValues.put(COL_9,bookingClass);
        db.update(TABLE_NAME, contentValues, "ID = ?",new String[] { id });
        return true;
    }

    public Integer deleteData (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?",new String[] {id});
    }
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null,null);

    }
}
