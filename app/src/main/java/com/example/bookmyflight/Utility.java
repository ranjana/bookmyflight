package com.example.bookmyflight;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class Utility {
    private static HashMap<Integer,String> providerDescription = new HashMap<>();
    private static ArrayList<String> sortlist = new ArrayList<>();
    private static DatabaseHelper myDb;
    public static final String MY_PREFS_NAME = "MyPrefsFile";



    public static void setTime(TextView textView, long time) {
        long totalseconds = time / 1000;
        long hour = totalseconds / (3600*24);
        long timeleft = totalseconds % 3600;
        long minutes = timeleft / 60;
        long seconds = timeleft % 60;
        textView.setText(""+hour+":"+minutes+":"+seconds);

    }

    public static HashMap<Integer,String> getEntry() {
        providerDescription.put(1,"Make my trip");
        providerDescription.put(2,"ClearTrip");
        providerDescription.put(3,"Yatra");
        providerDescription.put(4,"Musfir");
        return providerDescription;
    }

    public static ArrayList<String> getSpinnerList(){
        sortlist.add("price");
        sortlist.add("departureTime");
        sortlist.add("arrivaltime");
        return sortlist;
    }


public static void getlistOfData(Context context, ArrayList<Flight> flights){
    myDb = new DatabaseHelper(context);
    ArrayList<FilterData> filterData = new ArrayList<>();
    String originCode;
    String destinationCode;
    long departureTime;
    long arrivalTime;
    long fares;
    long providerId;
    String airlineCode;
    String bookingClass;
    for(Flight flight :flights){
        originCode = flight.getOriginCode();
        destinationCode = flight.getDestinationCode();
        departureTime = flight.getDepartureTime();
        arrivalTime = flight.getArrivalTime();
        airlineCode = flight.getAirlineCode();
        bookingClass = flight.getBookingClass();
        for(int i = 0;i<flight.getFares().size() ;i++){
            fares = flight.getFares().get(i).getFare();
            providerId = flight.getFares().get(i).getProviderId();
            filterData.add(new FilterData(originCode,destinationCode,departureTime,arrivalTime,fares,providerId,airlineCode,bookingClass));
            myDb.insertData(originCode,destinationCode,departureTime, arrivalTime,fares,providerId, airlineCode,bookingClass);
        }


    }
}

    public static ArrayList<FilterData> getAlldataFromDB(Context context) {
        myDb = new DatabaseHelper(context);
        ArrayList<FilterData> filterData = myDb.getAllData();
        return filterData;
    }



    public static void savedataInPreferance(Context context,String key, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public static boolean getBooleandataFromPreferance(Context context,String key) {
        SharedPreferences editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return editor.getBoolean(key,false);
    }

    public static void saveStringdataInPreferance(Context context,String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key,value);
        editor.commit();
    }

    public static String getStringdataFromPreferance(Context context,String key) {
        SharedPreferences editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return editor.getString(key,null);
    }

    public static boolean ifInternetAvailable(Activity activity){
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
        for(NetworkInfo netInfo : networkInfo){
            if(netInfo != null){
                if(netInfo.getTypeName().equalsIgnoreCase("WIFI")){
                    if(netInfo.isConnected()) {
                        Log.d("network available", "WIFI is available");
                        return true;
                    }
                }else if(netInfo.getTypeName().equalsIgnoreCase("MOBILE")){
                    if(netInfo.isConnected()) {
                        Log.d("network available", "WIFI is available");
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void deleteDB(Context context){
        myDb = new DatabaseHelper(context);
        myDb.deleteAll();
    }
}
